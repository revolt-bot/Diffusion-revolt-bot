var txt2img = {
	enable_hr: false,			//not_in_common
	denoising_strength: 0,
	firstphase_width: 0,		//not_in_common
	firstphase_height: 0,		//not_in_common
	hr_scale: 2,				//not_in_common
	hr_upscaler: 'string',		//not_in_common
	hr_second_pass_steps: 0,	//not_in_common
	hr_resize_x: 0,				//not_in_common
	hr_resize_y: 0,				//not_in_common
	prompt: "cat",
	styles: [ 'string' ],
	seed: -1,
	subseed: -1,
	subseed_strength: 0,
	seed_resize_from_h: -1,
	seed_resize_from_w: -1,
	sampler_name: "Euler a",
	batch_size: 1,
	n_iter: 1,
	steps: 20,
	cfg_scale: 7,
	width: 	512,
	height: 512,
	restore_faces: false,
	tiling: false,
	negative_prompt: "",
	eta: 0,
	s_churn: 0,
	s_tmax: 0,
	s_tmin: 0,
	s_noise: 1,
	override_settings: {},
	override_settings_restore_afterwards: true,
	script_args: [],
	sampler_index: "Euler a",
}


var img2img = { //img2img
		
		  init_images: "",	//different
		  resize_mode: 0,				//different
		  denoising_strength: 1,
		  image_cfg_scale: 0, 		//different
		  mask: "",					//different
		  mask_blur: 1,					//different
		  inpainting_fill: 1,			//different
		  inpaint_full_res: false,		//different
		  inpaint_full_res_padding: 0,	//different
		  inpainting_mask_invert: 0,	//different
		  initial_noise_multiplier: 1,	//different
		  prompt: "you forgot the prompt lol",
		  styles: [],
		  seed: -1,
		  subseed: -1,
		  subseed_strength: 0,
		  seed_resize_from_h: -1,		
		  seed_resize_from_w: -1,
		  sampler_name: "Euler a",
		  batch_size: 1,
		  n_iter: 1,
		  steps: 20,
		  cfg_scale: 30,
		  width: 512,
		  height: 512,
		  restore_faces: false,
		  tiling: false,
		  negative_prompt: "deformed",
		  eta: 0,
		  s_churn: 0,
		  s_tmax: 0,
		  s_tmin: 0,
		  s_noise: 1,
		  override_settings: {},
		  override_settings_restore_afterwards: true,
		  script_args: [],
		  sampler_index: "Euler",
		  include_init_images: true
	}
	
module.exports = {img2img,txt2img};
	
	
	



