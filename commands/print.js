const { Command } = require("../revolt-handler/main");
const { roleRequiredToMute, mutedRole } = require("../settings.json");
const Uploader = require("revolt-uploader");
const uploader = global.globalRevoltUploaderInstance;
const fs = require('fs');
const path = require('path');
const controller = new AbortController()
const signal = controller.signal
const axios = require('axios').default;
const defaults = require('./defaults.js');
const { download } = require("./download");
const { getMostRecentFile } = require("./download");
const AUTH = Buffer.from("LOGIN:PASSWORD").toString("base64");

const SDAPI = "127.0.0.1:7860"// Or the link for the SD endpoint



const prom= new Command({
	name: "prom",
	aliases: ["p", "prompt", "promp"],
	description: "What you want to see",
	requiredRoles: [[roleRequiredToMute], false],
});



const set_vae  = new Command({
	name: "set_vae",
	description: "Set a model #1",
	requiredRoles: [[roleRequiredToMute], false],
});


const setmodel  = new Command({
	name: "setmodel",
	description: "Set a model #1",
	requiredRoles: [[roleRequiredToMute], false],
});

const print = new Command({
	name: "print",
	aliases: "pri",
	description: "shows prompt and negative prompt",
	requiredRoles: [[roleRequiredToMute], false],
});

const gen = new Command({
	name: "gen",
	aliases: ["g", "generate"],
	description: "generates image based on prompt",
	requiredRoles: [[roleRequiredToMute], false],
});


const steps = new Command({
	name: "steps",
	description: "Ammount of time it takes to generate, 20 generates good enough, more than 50 it starts getting bad",
	requiredRoles: [[roleRequiredToMute], false],
});


const nprom	= new Command({
	name: "nprom",
	aliases: ["np", "negative_prompt", "negprom"],
	description: "What you dont want to see",
	requiredRoles: [[roleRequiredToMute], false],
});

const height = new Command({
	name: "height",
	aliases: ["h", "H", "Height"],
	description: "dimension Y of the image",
	requiredRoles: [[roleRequiredToMute], false],
});


const width = new Command({
	name: "width",
	aliases: ["w", "W", "Width"],
	description: "dimension X of the image",
	requiredRoles: [[roleRequiredToMute], false],
});
const gscale = new Command({
	name: "gscale",
	aliases: ["guidance_scale", "g_scale", "guiscale"],
	description: "guidance scale, how strong your prompt is",
	requiredRoles: [[roleRequiredToMute], false],
});


const seed = new Command({
	name: "seed",
	description: "Shows this message",
	requiredRoles: [[roleRequiredToMute], false],
});

const help = new Command({
	name: "help",
	description: "Shows this message",
	requiredRoles: [[roleRequiredToMute], false],
});


const mask = new Command({
	name: "mask",
	description: " image mask",
	requiredRoles: [[roleRequiredToMute], false],
});

const img = new Command({
	name: "img",
	description: "img image",
	requiredRoles: [[roleRequiredToMute], false],
});


const parse = new Command({
	name: "parse",
	description: "advanced options",
	requiredRoles: [[roleRequiredToMute], false],
});

const image = new Command({
	name: "image",
	description: "gets image",
	requiredRoles: [[roleRequiredToMute], false],
});
const sampler = new Command({
	name: "sampler",
	description: "pick a sampler 1-5",
	requiredRoles: [[roleRequiredToMute], false],
});



const getmodels  = new Command({
	name: "getmodels",
	description: "returns a file with a list of the models",
	requiredRoles: [[roleRequiredToMute], false],
});


var First_in_line; //the fist one to get generated
var Last_in_line = {}; //the current one


var path2 = "./outputs";

var Opt_list_queque = [];
const sampler_list = ["Euler ancestral","Euler","LMS","Heun","DPM 2","DPM 2 ancestral","DDIM","PLMS"];
var ready_to_generate=1;
var model_list =[];



async function send_options(data){
	return await axios.post(SDAPI+"/sdapi/v1/options",data,{'Content-Type': 'application/json',"auth": {
				username: "LOGIN",
				password: "PASSWORD"
			} })

}

function wait_for_file(fpath,image,message){
	
	
	buff = fs.readFileSync(fpath, {encoding: 'base64'});
	image =[buff]
	console.log("wait for file"+fpath);
	message.channel.sendMessage("file uploaded succesfully");

	}


mask.on("ran", (message, args) => {
	console.log("mask being ran")
	att=message.attachments;
	embend=message;

	imag=att[0];
	id=imag._id;
	fname=imag.filename;
	p1 = "https://autumn.revolt.chat/attachments/"
	url=p1+id+"/"+fname;
	
	download(url, "./inputs//"+fname)
	
	setTimeout(function(){
		buff = fs.readFileSync("./inputs//"+fname, {encoding: 'base64'});
		Last_in_line.mask=buff
		message.channel.sendMessage("file uploaded succesfully");
		},10000)
		
});


img.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	
	
	console.log("img  ran")
	att=message.attachments;
	embend=message;

	imag=att[0];
	id=imag._id;
	fname=imag.filename;
	p1 = "https://autumn.revolt.chat/attachments/"
	url=p1+id+"/"+fname;
	
	download(url, "./inputs//"+fname);
	console.log("imag");

	setTimeout(function(){
		buff = fs.readFileSync("./inputs//"+fname, {encoding: 'base64'});
		Last_in_line.init_images=[buff]
		message.channel.sendMessage("file uploaded succesfully");
		},10000)
	
});




sampler.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	let n= args.join(" ");
	nn= +n;
	Last_in_line.sampler =sampler_list[nn];
	message.channel.sendMessage("sampler set: "+sampler_list[nn]);
});

parse.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	
	let n = args.join(" ");
	data = JSON.parse(n);
	Object.keys(data).forEach(key => {
		if(!data[key])
			Last_in_line[key]=data[key]
	})

	});


steps.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	let n= args.join(" ");
	Last_in_line.steps = +n;
});

function handle_models(data,message){
	message.channel.sendMessage("Here is the model list:");
	for(key in data){
		model_list.push(data[key].title);
		
	}
	message.channel.sendMessage(model_list.join("---\n"));
	}

getmodels.on("ran", (message, args) => {


	fetch(SDAPI+"/sdapi/v1/sd-models",{
	method: "GET",
	headers: {
			"accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": "Basic " + AUTH
	},
	}).then(data => data.json()).then(data => handle_models(data,message))
	

})

setmodel.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	let n= args.join(" ");
	n = +n;
	if (n>model_list.length){
		message.channel.sendMessage("Invalid model number, use !getmodels then pick by the number");
		return;
	}
	data = {"sd_model_checkpoint":model_list[n]};
	send_options(data);
	
});

set_vae.on("ran", (message, args) => {
	let n= args.join(" ");
	
	message.channel.sendMessage("Loading VAE please wait a while before asking for a image");

	data = {"sd_vae":n};
	send_options(data);
});





seed.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	let n= args.join(" ");
	Last_in_line.seed = +n;
	message.channel.sendMessage("Seed set.");
});

height.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	let n= args.join(" ");
	n = +n;
	n=Math.round(n/64)*64;
	Last_in_line.height =n;
	console.log(n);
	if(!Last_in_line.width)
		Last_in_line.width=512;
	console.log(Last_in_line.width);
});

width.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	
	let n= args.join(" ");
	n = +n;
	n=Math.round(n/64)*64;
	Last_in_line.width =n;
	console.log(n);
	if(!Last_in_line.height)
		Last_in_line.height=512;
	console.log(Last_in_line.height);
});

gscale.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	let n= args.join(" ");
	n = +n;
	if (n>20){
		n=20;
		message.channel.sendMessage("Setting to 20, I think its too high, pm me if Im wrong tho");
	}
	Last_in_line.cfg_scale =n;
});

process.on("uncaughtException", (err) => {
    console.error(`Uncaught error:\n${err.stack}`);
	console.log(err);
});




print.on("ran", (message, args) => {
	let option = Opt_list_queque;
		while(option!==[]){
			o=option.pop();
			message.channel.sendMessage("#### prompt : "+`${o.prompt}`+", Your negative prompt is: "+`${o.negative_prompt}`);
	}
});

help.on("ran", (message, args) => {
	message.channel.sendMessage("print: "+`${print.description}`);
	message.channel.sendMessage("prompt, prom, p: "+`${prom.description}`);
	message.channel.sendMessage("width, w: "+`${width.description}`);
	message.channel.sendMessage("height, h: "+`${height.description}`);
	message.channel.sendMessage("nprom: "+`${nprom.description}`);
	message.channel.sendMessage("generate,gen, g: "+ `${gen.description}`);
	message.channel.sendMessage("steps: "+`${steps.description}`);
	message.channel.sendMessage("gscale: "+`${gscale.description}`);
	
});



prom.on("ran", (message, args) => {

	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	message.channel.sendMessage("prompt altered");
	let userprompt = args.join(" ");
	userprompt= userprompt.replaceAll("\"","");
	Last_in_line.prompt = userprompt;
	console.log(Last_in_line.prompt);
});


nprom.on("ran", (message, args) => {
	if (!args[0])
		return message.channel.sendMessage(
			"Invalid Arguments"
		);
	
	let userprompt = args.join(" ");
	message.channel.sendMessage("n prompt altered");
	message.channel.sendMessage(`${userprompt}`);
	Last_in_line.negative_prompt = userprompt;
});



function handle_img(image){
	imagess = image.data.images[0];
	var time = Number(new Date());
	var data = imagess.replace(/^data:image\/\w+;base64,/, '');
	pathh=path2+"/"+time+"_"+First_in_line.prompt.replaceAll(" ","_").replace(/[^\w\s\d]+/gi, '-');+"-sample.png";
	for(g=0;pathh.length>100;++g){
		pathh=path2+"/"+time+"_"+First_in_line.prompt.substr(g).replaceAll(" ","_").replace(/[^\w\s\d]+/gi, '-')+"-sample.png";
	}
	pathh=pathh+
	fs.writeFile(pathh, data, {encoding: 'base64'},function(err){
	console.log("error on handle img"+err);
	});
	}




async function send() {

	var a = defaults.txt2img;
	data ={};


	Object.keys(a).forEach(key => {
		if(First_in_line[key]==undefined){
			data[key]=a[key];
		} else{
			data[key]=First_in_line[key];
			}
	})
				
	return await axios.post(SDAPI+"/sdapi/v1/txt2img", 
		data,
		{  
			"accept": "application/json",
			"Content-Type": "application/json",
			"auth": {
				username: "LOGIN",
				password: "PASSWORD"
			}
			},
	
		).then(res => {
			
			if(typeof(res.data.images[0])!="string"){
				ready_to_generate=1;
				console.log("errooooooooooooooooooooooooooooooooooooooooooooooooo");
				console.log(res);
				return;
			}
			//var data = images.data.images[0].replace(/^data:image\/\w+;base64,/, '');
			pathh=path2+"/"+Date.now()+"_"+First_in_line.prompt.replaceAll(" ","_").replace(/[^\w\s\d]+/gi, '-').substr(-50)+"-sample.png";

			console.log(res);
			let data = Buffer.from(res.data.images.join(""), "base64").toString("binary");
			fs.writeFile(pathh, data, "binary", console.log);
			

			//fs.writeFile(pathh, data, {encoding: 'base64'},function(err){
			//console.log("error on handle img"+err);
			//console.log(images.data);
			});

}
 



async function send2() {
var a = defaults.img2img;
data ={};


Object.keys(a).forEach(key => {
	if(First_in_line[key]==undefined){
		data[key]=a[key];
	} else{
		data[key]=First_in_line[key];
		}
})



	return await axios.post(SDAPI+"/sdapi/v1/img2img",data,{'Content-Type': 'application/json',"auth": {
				username: "LOGIN",
				password: "PASSWORD"
			}})
	.then(images => handle_img(images))
}  













gen.on("ran", (message, args) => { // GEN GETS THE USER PROMPT OPTIONS!!!!!!!!!!!!!!!!!!!!!!!! would be like !!gr
    
	let a=0;
	var d=0;
	let n= args.join(" ");
	n = +n;
	Last_in_line.arg=n;

	if(Last_in_line.width*Last_in_line.height>1638400){
		message.channel.sendMessage("Change width or height, the image is too big");
		return;
	}
	CC=JSON.parse(JSON.stringify(Last_in_line));;
	Opt_list_queque.push(CC);
	
	if(ready_to_generate==1){
		ready_to_generate=2;
		First_in_line=Opt_list_queque.shift();
		if(First_in_line.arg==2){
			a =send2();
		} else{
			a=send();
			}
	    message.channel.sendMessage("Started generating");
    }
	
    if(ready_to_generate>1){
    	message.channel.sendMessage("Prompt added to queue, there are currently "+`${Opt_list_queque.length}`+" requests in the queque");
    }

});


fs.watch(path2, { recursive: true }, (eventType, filename) => { //When there is a change in the folder it REQUESTS!!!!!!!!!!!!!!
	console.log("fswatch"+filename+"  aa a "+eventType);
	if(eventType=="rename"){ //when a file is renamed
		if(Opt_list_queque.length>0){	
				First_in_line=Opt_list_queque.shift();
			
		
			if(First_in_line.arg==2){
				a =send2();
			} else{
				a=send();
				}
		}
		else{
			ready_to_generate=1;
		}

	}
  // could be either 'rename' or 'change'. new file event and delete
  // also generally emit 'rename'
})




exports.uploalatest =(uploader, channel) =>{
			console.log(`start of uploading Yay`);
			let a= path2;
			let namee = getMostRecentFile(a).file;
			channel.sendMessage("latest_pic: "+namee);
			a=a+"//"+namee;
			Promise.allSettled([
				uploader.uploadFile(a),
			  	]).then(attachments => { // we're using Promise.allSettled to asynchronously upload all of them
			    	attachments = attachments.map(attachment => attachment.value); // extracting the value from the promises
			    	// send the attachment to the channel
			    	channel.sendMessage({
					content: "Here is your file!",
					attachments: attachments // Note that attachments always has to be an array, even if you're only uploading one file
		    	});
   			 // All done!
 		});
	
};	

