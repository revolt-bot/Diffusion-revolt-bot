const fs = require('fs');
const http = require('http');
const https = require('https');	
const path = require('path');

exports.download=(url, filePath) => {
  const proto = !url.charAt(4).localeCompare('s') ? https : http;

  return new Promise((resolve, reject) => {
    const file = fs.createWriteStream(filePath);
    let fileInfo = null;

    const request = proto.get(url, response => {
      if (response.statusCode !== 200) {
        fs.unlink(filePath, () => {
          reject(new Error(`Failed to get '${url}' (${response.statusCode})`));
        });
        return;
      }

      fileInfo = {
        mime: response.headers['content-type'],
        size: parseInt(response.headers['content-length'], 10),
      };

      response.pipe(file);
    });

    // The destination stream is ended by the time it's called
    file.on('finish', () => resolve(fileInfo));

    request.on('error', err => {
      fs.unlink(filePath, () => reject(err));
    });

    file.on('error', err => {
      fs.unlink(filePath, () => reject(err));
    });

    request.end();
  });
}
// https://autumn.revolt.chat/attachments/pS8wloyPWZ50fuhb_Cyu-XCgNp2P__BWT1p5FAUnls/New%20Bitmap%20Image.png




const orderRecentFiles=(dir)=> {
  return fs
    .readdirSync(dir)
    .filter((file) => fs.lstatSync(path.join(dir, file)).isFile())
    .map((file) => ({ file, mtime: fs.lstatSync(path.join(dir, file)).mtime }))
    .sort((a, b) => b.mtime.getTime() - a.mtime.getTime());
}

exports.getMostRecentFile = (dir) => {
  const files = orderRecentFiles(dir);
  return files.length ? files[0] : undefined;
}

exports.nonewfile = (date1,path) =>{
	var audioFile =getMostRecentFile(path);
	var date2 = new Date(audioFile.mtime)
	if (date2>date1){
		return false;
	} else{
 		return true;
	}

	
}

